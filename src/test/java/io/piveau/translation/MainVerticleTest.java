package io.piveau.translation;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import io.piveau.translation.database.DatabaseService;
import io.piveau.translation.database.DatabaseVerticle;
import io.piveau.translation.http.HttpServerVerticle;
import io.piveau.translation.receiver.TranslationReceiverService;
import io.piveau.translation.receiver.TranslationReceiverVerticle;
import io.piveau.translation.request.TranslationRequestService;
import io.piveau.translation.request.TranslationRequestVerticle;
import io.piveau.translation.translation.TranslationService;
import io.piveau.translation.translation.TranslationVerticle;
import io.piveau.translation.util.ConfigConstant;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Testing all services")
@ExtendWith(VertxExtension.class)
public class MainVerticleTest {

  private static final Logger log = LoggerFactory.getLogger(MainVerticleTest.class.getName());

  private static EmbeddedPostgres pg;
  private static DatabaseService databaseService;
  private static TranslationRequestService translationRequestService;
  private static TranslationService translationService;
  private static TranslationReceiverService translationReceiverService;

  private static String exampleTranslation;
  private static String exampleRequest;
  private static WebClient client;


  @BeforeAll
  public static void setUp(Vertx vertx, VertxTestContext tc) {

    //Initiate the webclient here cause we will use it multiple times
    client = WebClient.create(vertx);

    //Create an embedded PostgresSql database in order to test the application
    try {
      log.debug("Starting mocked postgresDB.");
      //Starting the embedded database on port 5222 in order not to interfere with existing databases
      pg = EmbeddedPostgres.builder().setPort(5222).start();
      //Create the "test" user, password is not important
      pg.getPostgresDatabase().getConnection().createStatement().
        execute("CREATE USER test WITH PASSWORD 'jw8s0F4' CREATEDB;");
      //Create the actual database with owner "test" and name "postgres"
      pg.getPostgresDatabase().getConnection().createStatement()
        .execute(String.format("CREATE DATABASE %s OWNER %s ENCODING = 'utf8'", "test", "postgres"));
    } catch (SQLException ex) {
      log.error("There is a problem with the statement!" + ex.getMessage());
    } catch (IOException ex) {
      log.error("There is a problem with the database!" + ex.getMessage());
    }

    // Config file reading taken from MainVerticle might be better to overload ?!
    ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
      .setType("env")
      .setConfig(new JsonObject().put("keys", new JsonArray()
        .add(ConfigConstant.TRANSLATION_SERVICE)
        .add(ConfigConstant.ETRANSLATION)
        .add(ConfigConstant.DATABASE)
      ));

    ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
      .setType("file")
      .setConfig(new JsonObject().put("path", "conf/config.json"));

    ConfigRetriever retriever = ConfigRetriever
      .create(vertx, new ConfigRetrieverOptions()
        .addStore(fileStoreOptions)
        .addStore(envStoreOptions));

    //Once the config is loaded we need to put another port for the application to adress the postgres database
    retriever.getConfig(ar -> {
      JsonObject databaseObject = ar.result().getJsonObject(ConfigConstant.DATABASE);
      databaseObject.put("jdbc_url", "jdbc:postgresql://localhost:5222/postgres");
      //also we use the user "test" not the one used in the config
      databaseObject.put("user", "test");
      JsonObject config = ar.result();

      //in order to test a whole run we mock the e translation service
      JsonObject translationObject = ar.result().getJsonObject(ConfigConstant.ETRANSLATION);
      translationObject.put("mock", "true");

      config.remove(ConfigConstant.ETRANSLATION);
      config.put(ConfigConstant.ETRANSLATION, translationObject);

      config.remove(ConfigConstant.DATABASE);
      config.put(ConfigConstant.DATABASE, databaseObject);

      //deploy the databaseVerticle first then the rest (taken form Mainverticle)
      Future<String> dbVerticleDeployment = Future.future();
      vertx.deployVerticle(DatabaseVerticle.class.getName(), new DeploymentOptions().setConfig(config).setInstances(1), dbVerticleDeployment.completer());

      dbVerticleDeployment
        .compose(id1 -> {
          Future<String> translationRequestDeployment = Future.future();
          vertx.deployVerticle(TranslationRequestVerticle.class.getName(), new DeploymentOptions().setConfig(config).setInstances(1), translationRequestDeployment.completer());
          return translationRequestDeployment;
        })
        .compose(id2 -> {
          Future<String> translationReceiveDeployment = Future.future();
          vertx.deployVerticle(TranslationReceiverVerticle.class.getName(), new DeploymentOptions().setConfig(config).setInstances(1), translationReceiveDeployment.completer());
          return translationReceiveDeployment;
        })
        .compose(id3 -> {
          Future<String> httpVerticleDeployment = Future.future();
          vertx.deployVerticle(HttpServerVerticle.class.getName(), new DeploymentOptions().setConfig(config).setInstances(1), httpVerticleDeployment.completer());
          return httpVerticleDeployment;
        })
        .compose(id4 -> {
          Future<String> translationDeployment = Future.future();
          vertx.deployVerticle(TranslationVerticle.class.getName(), new DeploymentOptions().setConfig(config).setInstances(1), translationDeployment.completer());
          return translationDeployment;
        })
        .setHandler(ar2 -> {
          if (ar2.succeeded()) {
            //We want to test each verticles implementation so we create proxies to actually test their methods
            databaseService = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
            translationRequestService = TranslationRequestService.createProxy(vertx, TranslationRequestService.SERVICE_ADDRESS);
            translationService = TranslationService.createProxy(vertx, TranslationService.SERVICE_ADDRESS);
            translationReceiverService = TranslationReceiverService.createProxy(vertx, TranslationReceiverService.SERVICE_ADDRESS);

            //load exampleTranslation and exampleRequest for testing
            exampleTranslation = vertx.fileSystem().readFileBlocking("example_translation.json").toString();
            exampleRequest = vertx.fileSystem().readFileBlocking("example_translation_request.json").toString();

            tc.completeNow();
          } else {
            tc.failNow(ar2.cause());
          }
        });
    });

  }

  //Once all the test went through we close the database and all deployed services
  @AfterAll
  public static void tearDown(Vertx vertx, VertxTestContext tc) {
    try {
      pg.close();
      vertx.close(tc.completing());
    } catch (IOException e) {
      log.error("There was a problem closing the mock connection: " + e.getMessage());
    }
  }

  @Test
  @DisplayName("test_server_is_started")
  public void test_server_is_started(Vertx vertx, VertxTestContext tc) {
    log.debug("Runnning the test_server_is_started now!");
    vertx.createHttpClient().getNow(8080, "localhost", "/", response -> {
      //is the response 200 if we just send a get?
      tc.verify(() -> assertEquals(200, response.statusCode()));
      response.bodyHandler(body -> {
        //do we get any body bacK?
        tc.verify(() -> assertTrue(body.length() > 0));
        tc.completeNow();
      });
    });
  }

  @Test
  @DisplayName("test_succeding_run")
  public void test_succeding_run(Vertx vertx, VertxTestContext tc) {
    log.debug("Running test_succeding_run now!");

    //We create a mock translation requester and listen on a callback path for the translations
    HttpServer server = vertx.createHttpServer();

    Router router = Router.router(vertx);
    //this is NECESSARY in order to see the body of the incoming translation
    router.post().handler(BodyHandler.create());
    router.post("/callback").handler(handle -> {
      log.debug("We got a response!");
      //the translation always has the "id" key
      tc.verify(() -> assertTrue(handle.getBodyAsJson().containsKey("id")));
      handle.response().setStatusCode(200).end();
      //closing the server when finished
      server.close(handler -> {
        if (handler.succeeded())
          tc.completeNow();
      });
    });

    server
      .requestHandler(router)
      .listen(8222, ar -> {
        if (ar.succeeded()) {
          log.debug("Mock translation requester is running now!");
          //sending a translation request to the mocked application
          HttpRequest<Buffer> request = client.post(8080, "localhost", "/translation-service")
            .putHeader("Content-Type", "application/json");

          request.sendJsonObject(getExampleTranslationRequest(), ar2 -> {
            if (ar2.succeeded()) {
              log.debug("Example request was send!");
            } else {
              log.error("There was a problem sending the example request : " + ar2.cause().getMessage());
              tc.failNow(ar2.cause());
            }
          });

        } else {
          log.error("Could not start the mock translation requester", ar.cause());
          tc.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("test_response_to_incoming_false_request")
  public void test_response_to_incoming_false_request(Vertx vertx, VertxTestContext tc) {
    log.debug("Runnning the test_response_to_incoming_false_request now!");

    HttpRequest<Buffer> request = client.post(8080, "localhost", "/translation-service")
      .putHeader("Content-Type", "application/json");

    //response should be 400 if we send the incorrect request
    request.sendJsonObject(getIncorrectExampleTranslationRequest(), ar -> {
      if (ar.succeeded()) {
        tc.verify(() -> assertEquals(400, ar.result().statusCode()));
        tc.completeNow();
      } else {
        tc.failNow(ar.cause());
      }
    });
  }

  private JsonObject getExampleTranslationRequest() {
    return new JsonObject()
      .put("original_language", "en")
      .put("languages", new JsonArray().add("de").add("fi").add("pt").add("fr"))
      .put("callback", new JsonObject()
        .put("url", "http://localhost:8222/callback")
        .put("method", "POST")
        .put("payload", new JsonObject()
          .put("id", "test-dataset+test-catalog"))
        .put("headers", new JsonObject()
          .put("Authorization", "your-api-key")))
      .put("data_dict", new JsonObject()
        .put("description", "This is an example dataset."));
  }

  private JsonObject getIncorrectExampleTranslationRequest() {
    return new JsonObject()
      .put("MISTAKE", "de")
      .put("languages", new JsonArray().add("mt").add("ga").add("sv").add("nl"))
      .put("callback", new JsonObject()
        .put("url", "http://localhost")
        .put("method", "POST")
        .put("payload", new JsonObject()
          .put("id", "test-dataset+test-catalog"))
        .put("headers", new JsonObject()
          .put("Authorization", "your-api-key")))
      .put("data_dict", new JsonObject()
        .put("title", "Beispieldatensatz 2")
        .put("description", "Dies ist ein Beispiel Datensatz."));
  }

}
