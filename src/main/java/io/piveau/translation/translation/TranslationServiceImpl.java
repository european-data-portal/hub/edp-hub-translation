package io.piveau.translation.translation;

import io.piveau.translation.database.DatabaseService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;


public class TranslationServiceImpl implements TranslationService {

  private static final Logger log = LoggerFactory.getLogger(TranslationServiceImpl.class);

  public static final String CONFIG_ETRANSLATION_USER = "user";
  public static final String CONFIG_ETRANSLATION_APP = "application";
  public static final String CONFIG_ETRANSLATION_PASSWORD = "password";
  public static final String CONFIG_ETRANSLATION_URL = "e_translation_url";
  public static final String CONFIG_ETRANSLATION_CALLBACK = "callback_url";
  public static final String CONFIG_ETRANSLATION_SIMULTANOUS_TRANSLATIONS = "simultanous_translations";
  public static final String CONFIG_ETRANSLATION_REQUEST_FREQUENCY = "request_frequency";
  public static final String CONFIG_ETRANSLATION_TIMEOUT = "timeout";
  private static HashSet<String> completeTranslations;

  private final int simultanousTranslations;
  private final int requestFrequency;
  private final int timeout;
  private DatabaseService translationDb;
  private JsonObject config;
  private Vertx vertx;
  private WebClient client;

  public TranslationServiceImpl(Vertx vertx, JsonObject config, Handler<AsyncResult<TranslationService>> readyHandler) {
    this.translationDb = DatabaseService.createProxy(vertx, DatabaseService.SERVICE_ADDRESS);
    this.config = config;
    this.simultanousTranslations = this.config.getInteger(CONFIG_ETRANSLATION_SIMULTANOUS_TRANSLATIONS);
    this.requestFrequency = this.config.getInteger(CONFIG_ETRANSLATION_REQUEST_FREQUENCY);
    this.timeout = this.config.getInteger(CONFIG_ETRANSLATION_TIMEOUT);
    this.vertx = vertx;
    this.client = WebClient.create(this.vertx);
    completeTranslations = new HashSet<String>();
    readyHandler.handle(Future.succeededFuture(this));

    // Initial translation process
    this.translationDb.unsendedTranslationRequests(ar -> {});
    this.mainLoop();
    this.coronaUpdateLoop();
  }

  private void mainLoop() {
    this.vertx.setPeriodic(this.requestFrequency, handler -> {
      this.checkTranslationRequestStatus(ar -> {});
      this.identifyOldTranslations();
    });
  }

  private void coronaUpdateLoop() {
    int frequency = 1000 * 60 * 60 * 6;  // ms * s * min * h
    this.vertx.setPeriodic(frequency, handler -> {
      translationDb.coronaUpdate(ar -> {});
    });
  }

  private void identifyOldTranslations() {
    translationDb.findOldTranslation(dbResult -> {
      if (dbResult.result() != null) {
        String trId = dbResult.result().getString("tr_id");
        LocalDateTime sendedDate = LocalDateTime.parse(dbResult.result().getString("sended_date"));
        sendedDate = LocalDateTime.from(sendedDate);
        if (sendedDate.until(LocalDateTime.now(), ChronoUnit.MINUTES) >= this.timeout) {
          // found old translation
          translationDb.unsendOneTranslationRequest(trId, ar -> {
            if (ar.succeeded()) {
              log.debug("Found one outdated translation request with id " + trId);
              translationDb.deleteActiveTranslation(trId, deleteResult -> {
                if (deleteResult.failed()) {
                  log.error("Could not delete active translation marked as old translation.");
                }
              });
              translationDb.deleteTranslations(trId, deleteResult -> {
                if (deleteResult.failed()) {
                  log.error("Could not delete translations.");
                }
              });
            }
          });
        }
      }
    });
  }

  @Override
  public TranslationService checkTranslationRequestStatus(Handler<AsyncResult<Void>> resultHandler) {
    translationDb.getNumActiveTranslations(activeTranslationResult -> {
      if (activeTranslationResult.succeeded() && activeTranslationResult.result().getInteger("count") < simultanousTranslations) {
        log.debug("Active translations: " + activeTranslationResult.result().getInteger("count") + " of max " + simultanousTranslations + " allowed.");

        // free slots for more translations are available
        translationDb.getOldestTranslationRequest(dbResult -> {
          if (dbResult.succeeded()) {
            JsonObject translationRequest = dbResult.result();
            if (translationRequest == null) {
              // no translation request in queue
              log.debug("No waiting translation request available");
              resultHandler.handle(Future.succeededFuture());
            } else {
              // found waiting translation request in queue
              log.debug("Found waiting translation.");
              log.debug(translationRequest.encode());

              // get necessary fields
              final String trId = translationRequest.getString("tr_id");
              final String sourceLanguage = translationRequest.getString("original_language");
              final JsonArray targetLanguages = new JsonArray(translationRequest.getString("target_languages").replace('\'', '\"'));
              final JsonObject dataDict = new JsonObject(translationRequest.getString("data_dict"));

              // update awaited translation number
              final int expectedTrainslations = this.getNumberOfExpectedTranslations(dataDict, targetLanguages.size());
              log.debug("Expected translations: " + expectedTrainslations);
              if (expectedTrainslations == 0) {
                this.checkTranslationStatus(trId, checkingStatus -> {
                  if (checkingStatus.succeeded()) {
                    log.debug("No translations possible because of no text to translate.");
                  }
                });
              } else {
                translationDb.updateExpectedTranslation(trId, expectedTrainslations, reduceResult -> {
                  if (reduceResult.succeeded()) {
                    // start translation process for every snippet in translation request
                    this.startTranslationProcess(trId, sourceLanguage, targetLanguages, dataDict, sendResult -> {
                      if (sendResult.succeeded()) {
                        resultHandler.handle(Future.succeededFuture());
                      } else {
                        resultHandler.handle(Future.failedFuture(sendResult.cause()));
                      }
                    });
                  } else {
                    log.error("Could not add number of awaited translations in db.");
                  }
                });
              }
            }
          } else {
            log.error("Could not get an unsended translation request from database.");
            resultHandler.handle(Future.failedFuture(dbResult.cause()));
          }
        });
      } else {
        log.debug("Enough active translations at this moment.");
        resultHandler.handle(Future.succeededFuture());
      }
    });
    return this;
  }

  @Override
  public TranslationService checkTranslationStatus(String trId, Handler<AsyncResult<Void>> resultHandler) {
    // Check the number of awaited translations
    translationDb.getNumTranslations(trId, numResult -> {
      if (numResult.succeeded()) {
        // Check if translation is outdated
        if (numResult.result() == null) {
          log.debug("Receive translation which is no longer in processed");
          translationDb.deleteTranslations(trId, deleteResult -> {
            if (deleteResult.failed()) {
              log.warn("Could not delete outdated translation from relation translation", deleteResult.cause());
              resultHandler.handle(Future.failedFuture(deleteResult.cause()));
            } else {
              resultHandler.handle(Future.succeededFuture());
            }
          });
          return;
        }

        final int awaitedTranslationCount = numResult.result().getInteger("num_translations");

        // Check the number of actual received translations
        translationDb.getActualNumTranslations(trId, actualNumResult -> {
          if (actualNumResult.succeeded()) {
            final int actualTranslationCount = actualNumResult.result().getInteger("count");

            // check if translation completed
            if (awaitedTranslationCount == actualTranslationCount && !completeTranslations.contains(trId)) {
              completeTranslations.add(trId);
              resultHandler.handle(Future.succeededFuture());

              // translation process completed
              translationDb.getTranslations(trId, translationResult -> {
                this.sendTranslationToRequester(trId, translationResult.result(), sendingResult -> {
                  if (sendingResult.succeeded()) {
                    translationDb.deleteActiveTranslation(trId, deleteResult -> {
                    });
                  } else {
                    // translation could not send back to requester, try again
                    this.checkTranslationStatus(trId, handler -> {
                    });
                  }
                });
              });
            } else {
              resultHandler.handle(Future.failedFuture("Translation not complete yet."));
            }
          }
        });
      }
    });
    return this;
  }

  private void sendTranslationToRequester(String trId, JsonArray translations, Handler<AsyncResult<Void>> resultHandler) {
    this.translationDb.getTranslationRequest(trId, dbResult -> {
      if (dbResult.succeeded()) {
        JsonObject result = dbResult.result();
        String payloadString = result.getString("payload");
        JsonObject response = new JsonObject();
        if (payloadString == null) {
          response = this.buildResponseJson(trId, result.getString("original_language"), translations, null);
        } else {
          response = this.buildResponseJson(trId, result.getString("original_language"), translations, new JsonObject(payloadString));
        }

        // send translations back now / end of process
        String url = result.getString("callback_url");
        String auth = result.getString("callback_auth");
        JsonObject finalResponse = response;
        this.client
          .postAbs(url)
          .putHeader("Content-Type", "application/json")
          .putHeader("Authorization", auth)
          .sendJsonObject(response, ar -> {
            if (ar.succeeded()) {
              log.info("Send translation back to requester.");
              log.debug(finalResponse.toString());
              this.removeTranslation(result);
              completeTranslations.remove(trId);
              resultHandler.handle(Future.succeededFuture());
            } else {
              if (config.getString("mock").equals("true")) {
                //in mock modus we delete anyway because there is a high chance the requester is not availble/bugs out etc
                this.removeTranslation(result);
                completeTranslations.remove(trId);
              }
              log.error("Could not send translation back to requester.", ar.cause());
              resultHandler.handle(Future.failedFuture(ar.cause()));
            }
          });

      } else {
        log.error("Could not get translation request from database.");
        resultHandler.handle(Future.failedFuture(dbResult.cause()));
      }
    });
  }

  private void startTranslationProcess(String trId, String originalLanguage, JsonArray targetLanguages, JsonObject dataDict, Handler<AsyncResult<Void>> resultHandler) {
    translationDb.sendedTranslationRequest(trId, changeResult -> {
      if (changeResult.succeeded()) {
        this.translationDb.insertActiveTranslation(trId, insertResult -> {
          if (insertResult.succeeded()) {
            log.info("New active translations: " + trId);
            dataDict.forEach(snippet -> {
              JsonObject json = this.buildJson(trId, snippet.getValue().toString(), originalLanguage, targetLanguages, snippet.getKey());
              if (config.getString("mock").equals("true"))
                mockResponse(trId, resultHandler);
              else {
                this.sendTranslationRequestToEtranslation(json, sendResult -> {
                  if (sendResult.succeeded()) {
                    resultHandler.handle(Future.succeededFuture());
                  } else {
                    resultHandler.handle(Future.failedFuture(sendResult.cause()));
                  }
                });
              }
            });
          } else {
            log.error("Could not create a new active translation.");
          }
        });
      }
    });
  }

  private void mockResponse(String trId, Handler<AsyncResult<Void>> resultHandler) {

    //TODO Maybe put in some sort of delay

    JsonArray result = new JsonArray();
    result.add(new JsonArray().add("useless").add("hr").add("title").add("Primjer Dataset za 2."))
      .add(new JsonArray().add("useless").add("bg").add("title").add("Primjer Dataset za 2."))
      .add(new JsonArray().add("useless").add("fr").add("title").add("Primjer Dataset za 2."))
      .add(new JsonArray().add("useless").add("de").add("title").add("Primjer Dataset za 2."))
      .add(new JsonArray().add("useless").add("cs").add("title").add("Primjer Dataset za 2."))
      .add(new JsonArray().add("useless").add("hu").add("title").add("Primjer Dataset za 2."));

    this.sendTranslationToRequester(trId, result, sendingResult -> {
      if (sendingResult.succeeded()) {
        translationDb.deleteActiveTranslation(trId, deleteResult -> {
          if (deleteResult.succeeded())
            resultHandler.handle(Future.succeededFuture());
          else
            resultHandler.handle(Future.failedFuture("Problem deleting from DB!"));
        });
      } else
        resultHandler.handle(Future.failedFuture("Problem while mocking!"));
    });
  }

  private JsonObject buildResponseJson(String trId, String originalLanguage, JsonArray translations, JsonObject payload) {
    JsonObject response = new JsonObject().put("id", trId).put("original_language", originalLanguage).put("payload", payload);
    JsonObject translation = new JsonObject();
    translations.forEach(t -> {
      JsonArray part = (JsonArray) t;
      if (translation.containsKey(part.getString(1))) {
        translation.getJsonObject(part.getString(1)).put(part.getString(2), part.getString(3));
      } else {
        translation.put(part.getString(1), new JsonObject().put(part.getString(2), part.getString(3)));
      }
    });
    response.put("translation", translation);
    return response;
  }

  private void removeTranslation(JsonObject translationRequest) {
    String trId = translationRequest.getString("tr_id");
    String transmissionDate = translationRequest.getString("transmission_date");
    String finishedDate = LocalDateTime.now().toString();
    int duration = -1;
    String originalLanguage = translationRequest.getString("original_language");
    int numTargetLanguages = translationRequest.getInteger("num_translations");
    translationDb.deleteTranslationRequest(trId, ar -> {
      if (ar.failed()) {
        log.error("Could not delete translation request from database.");
      }
    });
    translationDb.deleteTranslations(trId, ar -> {
      if (ar.failed()) {
        log.error("Could not delete received translation from database.");
      }
    });
    translationDb.saveFinishedTranslation(trId, transmissionDate, finishedDate, duration, originalLanguage, numTargetLanguages, ar -> {
      if (ar.failed()) {
        log.error("Could not insert statistics about finished translations.");
      }
    });
  }

  private void sendTranslationRequestToEtranslation(JsonObject body, Handler<AsyncResult<HttpResponse>> resultHandler) {
    String url = this.config.getString(CONFIG_ETRANSLATION_URL);
    String applicationName = this.config.getString(CONFIG_ETRANSLATION_APP);
    String password = this.config.getString(CONFIG_ETRANSLATION_PASSWORD);
    this.vertx.executeBlocking(promise -> {
      // crappy old code from eTranslation (with using apache http client)
      try {
        // TODO update this code from eTranslation
        DefaultHttpClient client = new DefaultHttpClient();
        client.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(applicationName, password));
        HttpPost post = new HttpPost(url);
        post.setEntity(new StringEntity(body.encode(), ContentType.APPLICATION_JSON.getMimeType(), "UTF-8"));
        HttpClientParams.setRedirecting(post.getParams(), false);
        HttpResponse response = client.execute(post);
        log.debug("Status code: " + response.getStatusLine().getStatusCode());
        log.debug("Response code: " + EntityUtils.toString(response.getEntity()));
        promise.complete(response);
      } catch (Exception e) {
        resultHandler.handle(Future.failedFuture(e.getMessage()));
      }
    }, res -> {
      resultHandler.handle(Future.succeededFuture());
    });
  }

  private int getNumberOfExpectedTranslations(JsonObject dataDict, int numberOfTargetLanguages) {
    // TODO This code is nessessary until the translations from 2020-03-20 are finished.
    AtomicInteger counter = new AtomicInteger();
    dataDict.forEach(field -> {
      if (field.getValue().toString().length() > 0) {
        counter.addAndGet(numberOfTargetLanguages);
      }
    });
    return counter.get();
  }

  private JsonObject buildJson(String trId, String textToTranslate, String sourceLanguage, JsonArray targetLanguages, String textId) {
    JsonObject json = new JsonObject();
    json
      .put("priority", 0)
      .put("externalReference", trId + "+++" + textId)
      .put("callerInformation", new JsonObject()
        .put("application", this.config.getString(CONFIG_ETRANSLATION_APP))
        .put("username", this.config.getString(CONFIG_ETRANSLATION_USER)))
      .put("textToTranslate", textToTranslate)
      .put("sourceLanguage", sourceLanguage)
      .put("targetLanguages", targetLanguages)
      .put("domain", "SPD")
      .put("requesterCallback", this.config.getString(CONFIG_ETRANSLATION_CALLBACK) + "/et_success_handler")
      .put("errorCallback", this.config.getString(CONFIG_ETRANSLATION_CALLBACK) + "/et_error_handler")
      .put("destinations", new JsonObject());
    return json;
  }
}
